# -*- coding: utf-8 -*-
# By Kai Sauerwald

import scrapy
import codecs
import datetime

from Mensa.items import MensaItem
import json
import os
import re
from os import listdir
from os.path import isfile, join
import shutil
from sys import platform as _platform

# V = Vegetarisch
# S = Schwein
# G = Geflügel
# F = Fisch
# R = Rind
# W = Wild
# L = Lamm
# K = Kinderteller
# N = Vegan
# A = Fleisch aus artgerechter Haltung
def replacestuff(str):
        strs = str.split(',')
        ret = u'';
        first = True;
        for s in strs:
            if not first:
                ret = ret + u', ';
            first = False;
            ret = ret + s.replace("V", u'"Vegetarisch"').replace("S", u'"Schwein"').replace("G", u'"Geflügel"').replace("F", u'"Fisch"').replace("R", u'"Rind"').replace("W", u'"Wild"').replace("L", u'"Lamm"').replace("K", u'"Kinderteller"').replace("N", u'"Vegan"').replace("A", u'"Fleisch aus artgerechter Haltung"');
        return ret;

class MensaSpider(scrapy.Spider):
    name = "Mensa";
    # year = ""
    def __init__(self, *args, **kwargs):
        super(MensaSpider, self).__init__(*args, **kwargs)
        # extract from pipeline
        # truncate file and start json object
        file = codecs.open('mensa.json', 'w+', "utf-8")
        file.write("{")
        file.close()
        
    start_urls = [
        "http://www.stwdo.de/gastronomie/speiseplaene/hauptmensa/",
        "http://www.stwdo.de/gastronomie/speiseplaene/restaurant-calla/",
        "http://www.stwdo.de/gastronomie/speiseplaene/vital/",
        "http://www.stwdo.de/gastronomie/speiseplaene/galerie/",
        "http://www.stwdo.de/gastronomie/speiseplaene/food-fakultaet/",
        "http://www.stwdo.de/gastronomie/speiseplaene/archeteria-campus-sued/",
        "http://www.stwdo.de/gastronomie/speiseplaene/mensa-sued/",
        "http://www.stwdo.de/gastronomie/speiseplaene/kostbar/"
    ]
    def parse(self, response):
#         <tr class="even"> 
#         <td>Zucchini- Hackfleischpfanne (20,31)</td> 
#         <td class="center">R,K</td> 
#         <td class="center"> <a data-tooltip="Menü 1">
#         <img src="fileadmin/images/speiseplaene/menuekategorie/icon-menue-1.png" alt="Menü 1"></a> 
#         </td> 
#         </tr>
        file = codecs.open('mensa.json', 'a', "utf-8")
        file.truncate()
        
        cat = u"Unbekannt";
        if response.url == "http://www.stwdo.de/gastronomie/speiseplaene/hauptmensa/":
            cat = u'Hauptmensa';
        elif response.url == "http://www.stwdo.de/gastronomie/speiseplaene/restaurant-calla/":
            cat = u'Resturant Calla';
        elif response.url == "http://www.stwdo.de/gastronomie/speiseplaene/vital/":
            cat = u'Vital';
        elif response.url == "http://www.stwdo.de/gastronomie/speiseplaene/galerie/":
            cat = u'Galerie';
        elif response.url == "http://www.stwdo.de/gastronomie/speiseplaene/food-fakultaet/":
            cat = u'Food-Fakultät';
        elif response.url == "http://www.stwdo.de/gastronomie/speiseplaene/archeteria-campus-sued/":
            cat = u'Archeteria-Campus-Süd';
        elif response.url == "http://www.stwdo.de/gastronomie/speiseplaene/mensa-sued/":
            cat = u'Mensa-Süd';
        elif response.url == "http://www.stwdo.de/gastronomie/speiseplaene/kostbar/":
            cat = u'Kostbar';
            
        list = [];
        
        if cat != u'Hauptmensa':
            file.write(u',');
        file.write(u'"' + cat + '": [');
        first = True;
        result = response.xpath("//tr[@class='even' or @class='odd']");
        for idx, sel in enumerate(result):
            tds = sel.xpath(".//td");
            print "Ergebnis " + str(idx) + ":";
            file.write(("" if first else ",\n") + "{\n");
            first = False;
            item = MensaItem();
            item['mensa'] = cat;
            for i, td in enumerate(tds):
                if i == 0:
                    x = td.xpath(u"./text()");
                    if  len(x) != 0:
                        res = x[0].extract();
                        print str(i) + u": " + res;
                        item['gericht'] = res;
                        file.write(u'"Gericht" : "' + res.replace('"', '\\"') + u'"');
                if i == 1:
                    x = td.xpath("./text()");
                    if len(x) != 0:
                        res = x[0].extract();
                        print str(i) + u": " + res;
                        item['speiseart'] = res;
                        file.write(',\n"Speiseart" : [' + replacestuff(res) + ']');
                elif i == 2:
                    x = td.xpath(".//img/@alt");
                    if  len(x) != 0:
                        res = x[0].extract();
                        print str(i) + u": " + res;
                        item['menucat'] = res;
                        file.write(u',\n"Menükategorie" : "' + res + '"\n');
            print "\n";
            file.write("\n}");
            list.append(item);
        file.write("\n]\n")
        file.close();
        return list;

    def closed(spider, reason):
        file = codecs.open('mensa.json', 'a', 'utf-8');
        file.truncate()
        # end json object
        file.write('\n}')
        file.close()
